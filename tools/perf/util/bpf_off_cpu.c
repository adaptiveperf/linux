// SPDX-License-Identifier: GPL-2.0
#include "util/bpf_counter.h"
#include "util/debug.h"
#include "util/evsel.h"
#include "util/evlist.h"
#include "util/off_cpu.h"
#include "util/perf-hooks.h"
#include "util/record.h"
#include "util/session.h"
#include "util/target.h"
#include "util/cpumap.h"
#include "util/thread_map.h"
#include "util/cgroup.h"
#include "util/strlist.h"
#include <bpf/bpf.h>

#include "bpf_skel/off_cpu.skel.h"

#define MAX_STACKS  1024
#define MAX_PROC  4096

static struct off_cpu_bpf *skel;
static struct ring_buffer *rb;
static int written_bytes = 0;
static u64 sample_type, sid;
static struct perf_data_file *file;

struct off_cpu_val {
	u32 pid;
	u32 tgid;
	u32 stack_id;
	u32 state;
	u64 cgroup_id;
	u64 timestamp;
	u64 delta;
};

union off_cpu_data {
	struct perf_event_header hdr;
	u64 array[1024 / sizeof(u64) + (MAX_STACKS - 32)];
};

static int off_cpu_config(struct evlist *evlist)
{
	struct evsel *evsel;
	struct perf_event_attr attr = {
		.type	= PERF_TYPE_SOFTWARE,
		.config = PERF_COUNT_SW_BPF_OUTPUT,
		.size	= sizeof(attr), /* to capture ABI version */
	};
	char *evname = strdup(OFFCPU_EVENT);

	if (evname == NULL)
		return -ENOMEM;

	evsel = evsel__new(&attr);
	if (!evsel) {
		free(evname);
		return -ENOMEM;
	}

	evsel->core.attr.freq = 1;
	evsel->core.attr.sample_period = 1;
	/* off-cpu analysis depends on stack trace */
	evsel->core.attr.sample_type = PERF_SAMPLE_CALLCHAIN;

	evlist__add(evlist, evsel);

	free(evsel->name);
	evsel->name = evname;

	return 0;
}

static void off_cpu_start(void *arg)
{
	struct evlist *evlist = arg;
        struct timespec ts;

	/* update task filter for the given workload */
	if (!skel->bss->has_cpu && !skel->bss->has_task &&
	    perf_thread_map__pid(evlist->core.threads, 0) != -1) {
		int fd;
		u32 pid;
		u8 val = 1;

		skel->bss->has_task = 1;
		skel->bss->uses_tgid = 1;
		fd = bpf_map__fd(skel->maps.task_filter);
		pid = perf_thread_map__pid(evlist->core.threads, 0);
		bpf_map_update_elem(fd, &pid, &val, BPF_ANY);
	}

	clock_gettime(CLOCK_MONOTONIC, &ts);
	skel->bss->start_tstamp = ts.tv_sec * 1000000000UL + ts.tv_nsec;

	skel->bss->enabled = 1;
}

static void off_cpu_finish(void *arg __maybe_unused)
{
	skel->bss->enabled = 0;
	off_cpu_bpf__destroy(skel);
}

/* v5.18 kernel added prev_state arg, so it needs to check the signature */
static void check_sched_switch_args(void)
{
	struct btf *btf = btf__load_vmlinux_btf();
	const struct btf_type *t1, *t2, *t3;
	u32 type_id;

	type_id = btf__find_by_name_kind(btf, "btf_trace_sched_switch",
					 BTF_KIND_TYPEDEF);
	if ((s32)type_id < 0)
		goto cleanup;

	t1 = btf__type_by_id(btf, type_id);
	if (t1 == NULL)
		goto cleanup;

	t2 = btf__type_by_id(btf, t1->type);
	if (t2 == NULL || !btf_is_ptr(t2))
		goto cleanup;

	t3 = btf__type_by_id(btf, t2->type);
	/* btf_trace func proto has one more argument for the context */
	if (t3 && btf_is_func_proto(t3) && btf_vlen(t3) == 5) {
		/* new format: pass prev_state as 4th arg */
		skel->rodata->has_prev_state = true;
	}
cleanup:
	btf__free(btf);
}

static int (*process_earlier_events)(void *rec, u64 timestamp);
static void *cur_record;

static int handle_event(void *ctx __maybe_unused, void *ev_data,
			size_t data_sz __maybe_unused) {
	struct off_cpu_val *val = (struct off_cpu_val *)ev_data;
	union off_cpu_data data = {
		.hdr = {
			.type = PERF_RECORD_SAMPLE,
			.misc = PERF_RECORD_MISC_USER,
		},
	};
	int size;
	int stack = bpf_map__fd(skel->maps.stacks);

	int n = 1;  /* start from perf_event_header */
	int ip_pos = -1;
	int written_earlier_bytes = 0;

	if (sample_type & PERF_SAMPLE_IDENTIFIER)
		data.array[n++] = sid;
	if (sample_type & PERF_SAMPLE_IP) {
		ip_pos = n;
		data.array[n++] = 0;  /* will be updated */
	}
	if (sample_type & PERF_SAMPLE_TID)
		data.array[n++] = (u64)val->pid << 32 | val->tgid;
	if (sample_type & PERF_SAMPLE_TIME)
		data.array[n++] = val->timestamp;
	if (sample_type & PERF_SAMPLE_ID)
		data.array[n++] = sid;
	if (sample_type & PERF_SAMPLE_CPU)
		data.array[n++] = 0;
	if (sample_type & PERF_SAMPLE_PERIOD)
		data.array[n++] = val->delta;
	if (sample_type & PERF_SAMPLE_CALLCHAIN) {
		int len = 0;

		/* data.array[n] is callchain->nr (updated later) */
		data.array[n + 1] = PERF_CONTEXT_USER;
		data.array[n + 2] = 0;

		bpf_map_lookup_elem(stack, &val->stack_id, &data.array[n + 2]);
		while (data.array[n + 2 + len])
			len++;

		/* update length of callchain */
		data.array[n] = len + 1;

		/* update sample ip with the first callchain entry */
		if (ip_pos >= 0)
			data.array[ip_pos] = data.array[n + 2];

		/* calculate sample callchain data array length */
		n += len + 2;
	}
	if (sample_type & PERF_SAMPLE_CGROUP)
		data.array[n++] = val->cgroup_id;

	size = n * sizeof(u64);
	data.hdr.size = size;

	if (process_earlier_events) {
		written_earlier_bytes = process_earlier_events(cur_record, val->timestamp);

		if (written_earlier_bytes < 0) {
			pr_err("failed to process earlier perf events before writing off-CPU data\n");
			return -1;
		}

		written_bytes += written_earlier_bytes;
	}

	written_bytes += size;

	if (perf_data_file__write(file, &data, size) < 0) {
		pr_err("failed to write perf data, error: %m\n");
		return -1;
	}

	return 0;
}

int off_cpu_prepare(struct evlist *evlist, struct target *target,
		    struct record_opts *opts, int *off_cpu_fd)
{
	int err, fd, i;
	int ncpus = 1, ntasks = 1, ncgrps = 1;
	struct strlist *pid_slist = NULL;
	struct str_node *pos;

	if (off_cpu_config(evlist) < 0) {
		pr_err("Failed to config off-cpu BPF event\n");
		return -1;
	}

	skel = off_cpu_bpf__open();
	if (!skel) {
		pr_err("Failed to open off-cpu BPF skeleton\n");
		return -1;
	}

	/* don't need to set cpu filter for system-wide mode */
	if (target->cpu_list) {
		ncpus = perf_cpu_map__nr(evlist->core.user_requested_cpus);
		bpf_map__set_max_entries(skel->maps.cpu_filter, ncpus);
	}

	if (target->pid) {
		pid_slist = strlist__new(target->pid, NULL);
		if (!pid_slist) {
			pr_err("Failed to create a strlist for pid\n");
			return -1;
		}

		ntasks = 0;
		strlist__for_each_entry(pos, pid_slist) {
			char *end_ptr;
			int pid = strtol(pos->s, &end_ptr, 10);

			if (pid == INT_MIN || pid == INT_MAX ||
			    (*end_ptr != '\0' && *end_ptr != ','))
				continue;

			ntasks++;
		}

		if (ntasks < MAX_PROC)
			ntasks = MAX_PROC;

		bpf_map__set_max_entries(skel->maps.task_filter, ntasks);
	} else if (target__has_task(target)) {
		ntasks = perf_thread_map__nr(evlist->core.threads);
		bpf_map__set_max_entries(skel->maps.task_filter, ntasks);
	} else if (target__none(target)) {
		bpf_map__set_max_entries(skel->maps.task_filter, MAX_PROC);
	}

	if (evlist__first(evlist)->cgrp) {
		ncgrps = evlist->core.nr_entries - 1; /* excluding a dummy */
		bpf_map__set_max_entries(skel->maps.cgroup_filter, ncgrps);

		if (!cgroup_is_v2("perf_event"))
			skel->rodata->uses_cgroup_v1 = true;
	}

	if (opts->record_cgroup) {
		skel->rodata->needs_cgroup = true;

		if (!cgroup_is_v2("perf_event"))
			skel->rodata->uses_cgroup_v1 = true;
	}

	set_max_rlimit();
	check_sched_switch_args();

	err = off_cpu_bpf__load(skel);
	if (err) {
		pr_err("Failed to load off-cpu skeleton\n");
		goto out;
	}

	if (opts->user_off_cpu_freq == -1)
		skel->bss->sample_period = 1;
	else
		skel->bss->sample_period = 1000000000UL / opts->user_off_cpu_freq;

        skel->bss->wakeup_events = opts->off_cpu_buffering;

	if (target->cpu_list) {
		u32 cpu;
		u8 val = 1;

		skel->bss->has_cpu = 1;
		fd = bpf_map__fd(skel->maps.cpu_filter);

		for (i = 0; i < ncpus; i++) {
			cpu = perf_cpu_map__cpu(evlist->core.user_requested_cpus, i).cpu;
			bpf_map_update_elem(fd, &cpu, &val, BPF_ANY);
		}
	}

	if (target->pid) {
		u8 val = 1;

		skel->bss->has_task = 1;
		skel->bss->uses_tgid = 1;
		fd = bpf_map__fd(skel->maps.task_filter);

		strlist__for_each_entry(pos, pid_slist) {
			char *end_ptr;
			u32 tgid;
			int pid = strtol(pos->s, &end_ptr, 10);

			if (pid == INT_MIN || pid == INT_MAX ||
			    (*end_ptr != '\0' && *end_ptr != ','))
				continue;

			tgid = pid;
			bpf_map_update_elem(fd, &tgid, &val, BPF_ANY);
		}
	} else if (target__has_task(target)) {
		u32 pid;
		u8 val = 1;

		skel->bss->has_task = 1;
		fd = bpf_map__fd(skel->maps.task_filter);

		for (i = 0; i < ntasks; i++) {
			pid = perf_thread_map__pid(evlist->core.threads, i);
			bpf_map_update_elem(fd, &pid, &val, BPF_ANY);
		}
	}

	if (evlist__first(evlist)->cgrp) {
		struct evsel *evsel;
		u8 val = 1;

		skel->bss->has_cgroup = 1;
		fd = bpf_map__fd(skel->maps.cgroup_filter);

		evlist__for_each_entry(evlist, evsel) {
			struct cgroup *cgrp = evsel->cgrp;

			if (cgrp == NULL)
				continue;

			if (!cgrp->id && read_cgroup_id(cgrp) < 0) {
				pr_err("Failed to read cgroup id of %s\n",
				       cgrp->name);
				goto out;
			}

			bpf_map_update_elem(fd, &cgrp->id, &val, BPF_ANY);
		}
	}

	err = off_cpu_bpf__attach(skel);
	if (err) {
		pr_err("Failed to attach off-cpu BPF skeleton\n");
		goto out;
	}

	if (perf_hooks__set_hook("record_start", off_cpu_start, evlist) ||
	    perf_hooks__set_hook("record_end", off_cpu_finish, evlist)) {
		pr_err("Failed to attach off-cpu skeleton\n");
		goto out;
	}

	*off_cpu_fd = bpf_map__fd(skel->maps.off_cpu);
	rb = ring_buffer__new(*off_cpu_fd, handle_event, NULL, NULL);

	if (!rb) {
		pr_err("Failed to create ring buffer for off-cpu\n");
		goto out;
	}

	return 0;

out:
	off_cpu_bpf__destroy(skel);
	return -1;
}

int off_cpu_write(void *rec, struct perf_session *session,
		  int process_events(void *rec, u64 timestamp))
{
	int err;
	struct evsel *evsel;

	written_bytes = 0;
	cur_record = rec;
	file = &session->data->file;

	evsel = evlist__find_evsel_by_str(session->evlist, OFFCPU_EVENT);
	if (evsel == NULL) {
		pr_err("%s evsel not found\n", OFFCPU_EVENT);
		return 0;
	}

	sample_type = evsel->core.attr.sample_type;

	if (sample_type & ~OFFCPU_SAMPLE_TYPES) {
		pr_err("not supported sample type: %llx\n",
		       (unsigned long long)sample_type);
		return -1;
	}

	if (sample_type & (PERF_SAMPLE_ID | PERF_SAMPLE_IDENTIFIER)) {
		if (evsel->core.id)
			sid = evsel->core.id[0];
	}

	process_earlier_events = process_events;
	err = ring_buffer__consume(rb);

	if (err < 0) {
		pr_err("failed to read from ring buffer\n");
	}

	return written_bytes;
}
